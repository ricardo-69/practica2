package facciricardorivera.p.practica2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class Recibir_parametro extends AppCompatActivity {

    TextView textView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibir_parametro);
        textView2 = (TextView) findViewById(R.id.textView2);
        Bundle bundle = this.getIntent().getExtras();
        textView2.setText(bundle.getString("dato"));
    }
}
